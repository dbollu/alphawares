package fr.eql.bolludaniel.awares.entity;

import java.io.Serializable;

public class Town implements Serializable {
    private Long idTown;
    private String ville;
    private String pCode;

    public Town(Long Town, String town, String pCode) {
        this.idTown = Town;
        this.ville = town;
        this.pCode = pCode;
    }

    /// Getters ///
    public Long getIdTown() {
        return idTown;
    }
    public String getVille() {
        return ville;
    }
    public String getpCode() {
        return pCode;
    }

    /// Setters ///
    public void setIdTown(Long idTown) {
        this.idTown = idTown;
    }
    public void setVille(String ville) {
        this.ville = ville;
    }
    public void setpCode(String pCode) {
        this.pCode = pCode;
    }
}
