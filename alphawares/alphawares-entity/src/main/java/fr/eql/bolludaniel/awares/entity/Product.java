package fr.eql.bolludaniel.awares.entity;

import java.util.List;

public class Product {

    private String name;
    private Float price;
    private Long id;
    private List<String> keywords;
    private Integer qty;

    public Product(String name, Float price, Long id, List<String> keywords, Integer qty) {
        this.name = name;
        this.price = price;
        this.id = id;
        this.keywords = keywords;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }



}
