package fr.eql.bolludaniel.awares.entity;

import java.io.Serializable;

public class ClientAdr implements Serializable {

        private Long idAddress;
        private String address;
        private Town town;
        private Long idClient;


        public ClientAdr(Long idAdress, String address, Town town, Long idClient) {
            this.idAddress = idAdress;
            this.address = address;
            this.town = town;
            this.idClient = idClient;
        }

        /// Getters ///
        public Long getIdAddress() {
            return idAddress;
        }
        public String getAddress() {
            return address;
        }
        public Town getTown() {
            return town;
        }
        public Long getIdClient() {
            return idClient;
        }

        /// Setters ///
        public void setIdAddress(Long idAddress) {
            this.idAddress = idAddress;
        }
        public void setAdress(String address) {
            this.address = address;
        }
        public void setTown(Town town) {
            this.town = town;
        }
        public void setIdClient(Long idClient) {
            this.idClient = idClient;
        }




}
