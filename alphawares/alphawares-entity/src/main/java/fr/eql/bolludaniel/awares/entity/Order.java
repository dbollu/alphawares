package fr.eql.bolludaniel.awares.entity;

import java.io.Serializable;
import java.util.List;

public class Order implements Serializable {

    private Long ordId;
    private List<OrderLine> orderLines;
    private Float price;

    public Order(Long ordId, List<OrderLine> orderLines, Float price) {
        this.ordId = ordId;
        this.orderLines = orderLines;
        this.price = price;
    }

    /// Getters ///
    public Long getOrdId() {
        return ordId;
    }
    public List<OrderLine> getOrderLines() {
        return orderLines;
    }
    public Float getPrice() {
        return price;
    }

    /// Setters ///
    public void setOrdId(Long ordId) {
        this.ordId = ordId;
    }
    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }
    public void setPrice(Float price) {
        this.price = price;
    }

}
