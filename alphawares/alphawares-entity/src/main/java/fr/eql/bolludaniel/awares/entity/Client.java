package fr.eql.bolludaniel.awares.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Client implements Serializable {

    private Long clientId;
    private String firstName;
    private String lastName;
    private LocalDate birthdate;
    private String email; // This also serves as the account's Login
    private String password;
    private String phone;
    private LocalDate signInDate;
    private LocalDate signOutDate;
    private ClientAdr address;
    private List<Order> orders;

    public Client(Long clientId, String firstName, String lastName, LocalDate birthdate, String email, String password, String phone, LocalDate signInDate) {
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.signInDate = signInDate;
    }

    /// Getters ///
    public Long getClientId() {
        return clientId;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public LocalDate getBirthdate() {
        return birthdate;
    }
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }
    public LocalDate getSignInDate() {
        return signInDate;
    }
    public LocalDate getSignOutDate() {
        return signOutDate;
    }
    public ClientAdr getClientAdresses() {
        return address;
    }
    public List<Order> getOrders() {
        return orders;
    }

    /// Setters ///
    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setSignInDate(LocalDate signInDate) {
        this.signInDate = signInDate;
    }
    public void setSignOutDate(LocalDate signOutDate) {
        this.signOutDate = signOutDate;
    }
    public void setAddress(ClientAdr a){
        this.address = a;
    }
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("****Client Information****\r\n");
        sb.append("ID: ").append(clientId).append("\r\n");
        sb.append("First Name: ").append(firstName).append("\r\n");
        sb.append("Last Name: ").append(lastName).append("\r\n");
        sb.append("Birth Date: ").append(birthdate).append("\r\n");
        sb.append("E-Mail address: ").append(email).append("\r\n");

        sb.append("Signed In: ").append(signInDate).append("\r\n");

        if (signOutDate != null) {
            sb.append("Signed Out: ").append(signOutDate);
        }
        else{
            sb.append("Still active");
        }

        return sb.toString();
    }
}

