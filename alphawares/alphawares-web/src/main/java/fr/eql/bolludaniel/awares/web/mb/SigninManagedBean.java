package fr.eql.bolludaniel.awares.web.mb;

import fr.eql.bolludaniel.awares.business.SignInBusiness;
import fr.eql.bolludaniel.awares.entity.Employee;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@ManagedBean(name = "mbSignin")
@SessionScoped
public class SigninManagedBean {

	/*
	*
	* Attributes for the creation of a new Employee object.
	*
	* Annotation legend
	* @NotNull: returns an error if the field is null
	* @Size: returns its message field as an error if any of the predicates are false
	* @Pattern: returns its message field as an error if the variable it annotates is not matching the regular expression in its regexp field
	*
	* */
	@NotNull
	@Size(min = 1, message = "Veuillez entrer un Prénom")
	private String newEmployeeFirstname;
	@NotNull
	@Size(min = 1, message = "Veuillez entrer un Nom")
	private String newEmployeeLastname;
	@NotNull(message = "Veuillez entrer une date de naissance")
	private Date newEmployeeBirthdate;
	@NotNull
	@Pattern(regexp = ".+@.+[\\.].+", message = "Veuillez entrer un email valide (---@---.---)")
	private String newEmployeeEmail;
	@NotNull(message = "Veuillez entrer un mot de passe d'au moins 6 caractères")
	@Size(min = 6, message = "Veuillez entrer un mot de passe d'au moins 6 caractères")
	private String newEmployeePassword;


	@EJB private SignInBusiness signInBusiness;

	/// Methods ///

	/**
	 * Inserts a new employee into the database.
	 * @return the URL that will be followed depending on the success of the operation:
	 *     SUCCESS : Back to the <code>index.xhtml</code>
	 *     FAILURE : Stay on this page, any errors that caused the insertion to fail are highlighted
	 */
	public String insertEmployee(){
		String forward;

		Employee newEmployee = new Employee(
				null,
				newEmployeeFirstname,
				newEmployeeLastname,
				newEmployeeBirthdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
				newEmployeeEmail,
				newEmployeePassword,

				LocalDate.now()
		);



		boolean result = signInBusiness.insertEmployee(newEmployee);

		if (result) {
			forward = "/login.xhtml?faces-redirect=true";
		} else {
			forward = "/inscription.xhtml?faces-redirect=false";
		}
		return forward;
	}



	/// Getters ///
	public String getNewEmployeeFirstname() {
		return newEmployeeFirstname;
	}
	public String getNewEmployeeLastname() {
		return newEmployeeLastname;
	}
	public Date getNewEmployeeBirthdate() {
		return newEmployeeBirthdate;
	}
	public String getNewEmployeeEmail() {
		return newEmployeeEmail;
	}
	public String getNewEmployeePassword() {
		return newEmployeePassword;
	}

	/// Setters ///
	public void setNewEmployeeFirstname(String newEmployeeFirstname) {
		this.newEmployeeFirstname = newEmployeeFirstname;
	}
	public void setNewEmployeeLastname(String newEmployeeLastname) {
		this.newEmployeeLastname = newEmployeeLastname;
	}
	public void setNewEmployeeBirthdate(Date newEmployeeBirthdate) {
		this.newEmployeeBirthdate = newEmployeeBirthdate;
	}
	public void setNewEmployeeEmail(String newEmployeeEmail) {
		this.newEmployeeEmail = newEmployeeEmail;
	}
	public void setNewEmployeePassword(String newEmployeePassword) {
		this.newEmployeePassword = newEmployeePassword;
	}

}
