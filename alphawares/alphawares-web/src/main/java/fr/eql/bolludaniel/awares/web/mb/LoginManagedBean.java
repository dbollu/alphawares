package fr.eql.bolludaniel.awares.web.mb;

import fr.eql.bolludaniel.awares.business.LoginBusiness;
import fr.eql.bolludaniel.awares.entity.Employee;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@ManagedBean(name = "mbLogin")
@SessionScoped
public class LoginManagedBean implements Serializable {


	@NotNull
	@Pattern(regexp = ".+@.+[\\.].+", message = "Veuillez entrer un email valide (---@---.---)")
	private String login;
	@NotNull
	private String password;

	private Employee connectedEmployee;


	@EJB
	private LoginBusiness loginBusiness;

	/// Methods ///

	/**
	 * Checks if credentials are matching the database.
	 * @return the url of the page to go to, depending on the status of the operation:
	 * <ul>
	 *     <li>SUCCESS: The user is redirected to their account page.</li>
	 *     <li>FAILURE: The user remains on the <code>login.xhtml</code> page, and any cause of failure are highlighted.</li>
	 * </ul>
	 */
	public String connect(){
		String forward;
		connectedEmployee = loginBusiness.login(login, password);
		if (connectedEmployee != null){
			forward = "/account.xhtml?faces-redirect=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Identifiant et/ou mot de passe incorrect(s)",
					""
			);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
			forward = "/login.xhtml?faces-redirect=false";
		}
		return forward;
	}

	/**
	 * Ends current session and disconnects the user.
	 * @return The URL to go to the home page.
	 */
	public String disconnect() {
		closeSession();
		login = "";
		password = "";
		connectedEmployee = null;
		return "/index.xhtml?faces-redirect=true";
	}

	/**
	 * Invalidates the current session.
	 * */
	public void closeSession(){
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(false);
		session.invalidate();
	}

	/**
	 * Returns whether the user is connected or not.
	 *
	 * @return <code>true</code> if the user is connected, <code>false</code> if they're not.
	 * */
	public boolean isConnected() {
		return connectedEmployee != null;
	}

	/**
	 * Returns the user's full name if they're connected. If they're not, a guest label is returned.
	 *
	 * @return the user's full name, composed of their <code>firstname</code> and <code>lastname</code> fields if they're connected.
	 * Otherwise, returns a generic Guest label.
	 * */
	public String visitorOrName(){
		String label;
		if (isConnected()){
			label = connectedEmployee.getFirstname() + " " + connectedEmployee.getLastname();
		} else {
			label = "Visiteur";
		}
		return label;
	}

	/**
	 * Used by all actions that require authentication. If an user is connected, this does nothing, but if they're not, any call
	 * to <code>authorize()</code> will redirect them to the <code>login.xhtml</code> page.
	 * */
	public void authorize() {
		FacesContext context = FacesContext.getCurrentInstance();
		ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler)
				context.getApplication().getNavigationHandler();
		if (!isConnected()) {
			handler.performNavigation("/login.xhtml?faces-redirect=true");
		}
	}

	/// Getters ///
	public String getLogin() {
		return login;
	}
	public String getPassword() {
		return password;
	}
	public Employee getConnectedEmployee() {
		return connectedEmployee;
	}

	/// Setters ///
	public void setLogin(String login) {
		this.login = login;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setConnectedEmployee(Employee connectedEmployee) {
		this.connectedEmployee = connectedEmployee;
	}
}
