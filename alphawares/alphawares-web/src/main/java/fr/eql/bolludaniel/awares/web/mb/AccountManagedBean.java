	// Gets the Employee which is connected on the website
	@ManagedProperty("#{mbLogin.connectedEmployee}")
	private Employee connectedEmployee;

	private Employee updatedEmployee;

	// Boolean which is used to set the inputText disability, depending on the method modifyFields()
	boolean modifying;

	// Attributs pour nouveau employee, avec validation + messages en cas de non respect des validations
	@NotNull
	@Size(min = 1, message = "Veuillez entrer un Prénom")
	private String employeeNewFirstname;
	@NotNull
	@Size(min = 1, message = "Veuillez entrer un Nom")
	private String employeeNewLastname;
	@NotNull(message = "Veuillez entrer un mot de passe d'au moins 6 caractères")
	@Size(min = 6, message = "Veuillez entrer un mot de passe d'au moins 6 caractères")
	private String employeeNewPassword;

	@EJB
	private AccountBusiness accountBusiness;

	/// Methods ///


	@PostConstruct
	public void init(){
		modifying = true;

		employeeNewFirstname = connectedEmployee.getFirstname();
		employeeNewLastname = connectedEmployee.getLastname();
		employeeNewPassword = connectedEmployee.getPassword();

	}

	/**
	 * Used to toggle the <code>modifying</code> flag. Said flag is returned afterwards.
	 *
	 * @return <code>true</code> if the <code>modifying</code> flag was set to <code>false</code>
	 * before calling this method, <code>false</code> otherwise.
	 */
	public boolean modifyFields(){
		return(modifying = !modifying);
	}

	/**
	 * Updates the currently connected employee, and redirects the user to either the index page, or the account page
	 * */
	public String updateConnectedEmployee(){
		String forward;
		// Defining the updated employee
		updatedEmployee = connectedEmployee;
		// Getting the information in the fields from account.xhtml
		updatedEmployee.setFirstname(employeeNewFirstname);
		updatedEmployee.setLastname(employeeNewLastname);
		updatedEmployee.setPassword(employeeNewPassword);


		// Updating database
		boolean isUpdated = accountBusiness.updateConnectedEmployee(updatedEmployee);

		if (isUpdated){
			forward = "/account.xhtml?faces-redirect=true";
		} else {
			forward = "/index.xhtml?faces-redirect=true";
		}
		return forward;
	}

	public String closeAccount(){
		String forward;
		boolean isDateInserted = accountBusiness.closeAccount(connectedEmployee);
		if (isDateInserted){
			HttpSession session = (HttpSession) FacesContext
					.getCurrentInstance()
					.getExternalContext()
					.getSession(true);
			session.invalidate();
			connectedEmployee = null;
			forward = "/index.xhtml?faces-redirect=true";
		} else {
			forward = "/account.xhtml?faces-redirect=false";
		}
		return forward;
	}




	/// Getters ///
	public boolean getModifying() {
		return modifying;
	}
	public Employee getConnectedEmployee() {
		return connectedEmployee;
	}
	public String getEmployeeNewFirstname() {
		return employeeNewFirstname;
	}
	public String getEmployeeNewLastname() {
		return employeeNewLastname;
	}
	public String getEmployeeNewPassword() {
		return employeeNewPassword;
	}

	/// Setters ///
	public void setIdModifying(boolean modifying) {
		this.modifying = modifying;
	}
	public void setConnectedEmployee(Employee connectedEmployee) {
		this.connectedEmployee = connectedEmployee;
	}
	public void setEmployeeNewFirstname(String employeeNewFirstname) {
		this.employeeNewFirstname = employeeNewFirstname;
	}
	public void setEmployeeNewLastname(String employeeNewLastname) {
		this.employeeNewLastname = employeeNewLastname;
	}
	public void setEmployeeNewPassword(String employeeNewPassword) {
		this.employeeNewPassword = employeeNewPassword;
	}
