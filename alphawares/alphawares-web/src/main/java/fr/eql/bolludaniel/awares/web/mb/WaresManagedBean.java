package fr.eql.bolludaniel.awares.web.mb;


import fr.eql.bolludaniel.awares.business.WaresBusiness;
import fr.eql.bolludaniel.awares.entity.Product;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name="mbWares")
@ViewScoped
public class WaresManagedBean implements Serializable {

	@EJB private WaresBusiness waresBusiness;

	List<Product> products;
	
	public List<Product> getDisplayProducts() {
		products = waresBusiness.displayProducts();
		return products;
	}
}