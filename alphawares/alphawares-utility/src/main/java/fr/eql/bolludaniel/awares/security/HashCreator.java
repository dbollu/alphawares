package fr.eql.bolludaniel.awares.security;

/*
*
* The following source code has been taken from git repository
* https://github.com/thombergs/code-examples/blob/master/java-hashes/src/main/java/hashing/HashCreator.java
* and modified to adapt to the needs of the project.
*
* */


import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;


public class HashCreator {

	private String convertToHex(final byte[] messageDigest) {
		BigInteger bigint = new BigInteger(1, messageDigest);
		String hexText = bigint.toString(16);
		while (hexText.length() < 32) {
			hexText = "0".concat(hexText);
		}
		return hexText;
	}

	/* use this method to hash the passwords */
	/**
	 *
	 * Generates a strong password hash using the PBKDF2WithHmacSHA1 algorithm, coupled with a salting method
	 * to protect against dictionnary attacks and rainbow tables.
	 *
	 * */
	public String generateStrongPasswordHash(final String password)
			throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException {
		int iterations = 1000;
		byte[] salt = createSalt();

		byte[] hash = createPBEHash(password, iterations, salt, 64); // skf.generateSecret(spec).getEncoded();

		return iterations + ":" + convertToHex(salt) + ":" + convertToHex(hash);
	}

	//Create salt
	private byte[] createSalt() throws NoSuchAlgorithmException, NoSuchProviderException {
		//Always use a SecureRandom generator for random salt
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
		//Create array for salt
		byte[] salt = new byte[16];
		//Get a random salt
		sr.nextBytes(salt);
		//return salt
		return salt;
	}


	/* use this method to validate passwords */
	/**
	 * Checks the input password against the stored password hash
	 *
	 * @return true if the password is correct, false otherwise.
	 * */
	public boolean validatePassword(final String originalPassword, final String storedPasswordHash)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		// Split the string by :
		String[] parts = storedPasswordHash.split(":");

		// Extract iterations, salt, and hash
		// from the stored password hash
		int iterations = Integer.parseInt(parts[0]);
		byte[] salt = convertToBytes(parts[1]);
		byte[] hash = convertToBytes(parts[2]);

		byte[] originalPasswordHash = createPBEHash(originalPassword, iterations, salt, hash.length);

		int diff = hash.length ^ originalPasswordHash.length; // do a bitwise XOR
		for (int i = 0; i < hash.length && i < originalPasswordHash.length; i++) {
			diff |= hash[i] ^ originalPasswordHash[i]; // bitwise OR of a bitwise XOR
		}
		return diff == 0; // if diff is not zero, the password is incorrect.
	}

	private byte[] createPBEHash(final String password, final int iterations, final byte[] salt, final int keyLength)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		PBEKeySpec spec = new PBEKeySpec(password.toCharArray(),
				salt, iterations, keyLength * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		return skf.generateSecret(spec).getEncoded();
	}

	private byte[] convertToBytes(final String hex) throws NoSuchAlgorithmException {
		byte[] bytes = new byte[hex.length() / 2];

		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = Integer.valueOf(hex.substring(2 * i, 2 * i + 2), 16).byteValue();
		}
		return bytes;
	}
}
