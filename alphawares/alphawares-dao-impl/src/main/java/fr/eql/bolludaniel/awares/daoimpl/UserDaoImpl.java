package fr.eql.bolludaniel.awares.daoimpl;

import fr.eql.bolludaniel.awares.dao.ClientAdrDao;
import fr.eql.bolludaniel.awares.dao.UserDao;
import fr.eql.bolludaniel.awares.datasource.AlphaWaresDataSource;
import fr.eql.bolludaniel.awares.entity.Client;
import fr.eql.bolludaniel.awares.entity.ClientAdr;
import fr.eql.bolludaniel.awares.entity.Employee;
import fr.eql.bolludaniel.awares.security.HashCreator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.time.LocalDate;

@Remote(UserDao.class)
@Stateless
public class UserDaoImpl implements UserDao {

	//get the singleton instance of Logger
	private static final Logger logger = LogManager.getLogger();

	ClientAdrDao addressDao = new ClientAdrDaoImpl();

	private static final String QUERY_DISP_CLIENT_INFO =
			"SELECT * FROM client WHERE firstname = ? AND lastname = ? ";

	private static final String QUERY_DISP_EMPLOYEE_INFO =
			"SELECT * FROM employee WHERE firstname = ? AND lastname = ? ";

	private static final String QUERY_INSERT_CLIENT =
			"INSERT INTO client (firstname, lastname, birthdate, email, hashed_password, signin, signout) VALUES(?, ?, ?, ?, ?, ?, NULL)";

	private static final String QUERY_INSERT_EMPLOYEE =
			"INSERT INTO employee (firstname, lastname, birthdate, email, hashed_password, signin, signout, role) VALUES(?, ?, ?, ?, ?, ?, NULL, ?)";

	private static final String QUERY_INSERT_ADRESSE = "INSERT INTO address (address, id_city, id_client) VALUES (?,?,?)";

	private static final String QUERY_LOGIN_CLIENT = "SELECT * FROM client WHERE mail = ? AND password = ?";

	private static final String QUERY_INSERT_SIGNOUT_DATE_CLIENT =
			"UPDATE client SET signout = ? WHERE id_client = ?";

	private static final String QUERY_INSERT_SIGNOUT_DATE_EMPLOYEE =
			"UPDATE employee SET signout = ? WHERE id_employee = ?";

	private static final String QUERY_UPDATE_CLIENT =
			"UPDATE client SET nom = ?, prenom = ?, password = ? WHERE id_client = ?";

	private static final String QUERY_UPDATE_EMPLOYEE =
			"UPDATE employee SET nom = ?, prenom = ?, password = ? WHERE id_employee = ?";

	private final DataSource dataSource = new AlphaWaresDataSource();

	/**
	 * Sends a query to the underlying database to fetch the informations of a <code>Client</code>
	 * that is identified by the argument <code>firstname</code> and <code>lastname</code>.
	 *
	 * @param firstname the first name of the client
	 * @param lastname the last name of the client
	 * @return <code>null</code> if no match is found, the client's informations otherwise.
	 */
	@Override
	public Client findClientInformations(String firstname, String lastname) {

		Client client = null;
		try (Connection conn = dataSource.getConnection()) {

			PreparedStatement ps = conn.prepareStatement(QUERY_DISP_CLIENT_INFO);
			ps.setString(1, firstname);
			ps.setString(2, lastname);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Long id = rs.getLong("id_client");
				client = new Client(
						id,
						rs.getString("lastname"),
						rs.getString("firstname"),
						rs.getDate("birthdate").toLocalDate(),
						rs.getString("mail"),
						rs.getString("hashed_password"),
						rs.getDate("signin").toLocalDate(),
						new ClientAdr(addressDao.getAddressInformations(id))
				);
			}

		} catch (SQLException e) {
			logger.error("An error occurred while trying to access the database.", e);
		}

		return client;
	}

	/**
	 * Sends a query to the underlying database to fetch the informations of an <code>Employee</code>
	 * that is identified by the argument <code>firstname</code> and <code>lastname</code>.
	 *
	 * @param firstname the first name of the employee
	 * @param lastname the last name of the employee
	 * @return <code>null</code> if no match is found, the employee's informations otherwise.
	 */
	@Override
	public Employee findEmployeeInformations(String firstname, String lastname) {
		Employee emp = null;
		try (Connection conn = dataSource.getConnection()) {

			PreparedStatement ps = conn.prepareStatement(QUERY_DISP_EMPLOYEE_INFO);
			ps.setString(1, firstname);
			ps.setString(2, lastname);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Long id = rs.getLong("id_employee");
				emp = new Employee(
						id,
						rs.getString("lastname"),
						rs.getString("firstname"),
						rs.getDate("birthdate").toLocalDate(),
						rs.getString("mail"),
						rs.getString("hashed_password"),
						rs.getDate("signin").toLocalDate()

				);
			}

		} catch (SQLException e) {
			logger.error("An error occurred while trying to access the database.", e);
		}

		return emp;
	}

	/**
	 * Inserts a new <code>Client</code> into the underlying database, based on the given arguments.
	 *
	 * @param client the client to insert
	 * @return <code>true</code> if the operation succeeded, <code>false</code> if it failed.
	 */
	@Override
	public boolean insertClient(Client client, ClientAdr adr) {
		boolean isInserted = false;

		try(Connection conn = dataSource.getConnection()){
			conn.setAutoCommit(false);

			long idClient = clientStatementExecution(adr, client, conn);

			if (idClient > 0) {
				boolean adresseInserted = addressStatementExecution(adr, conn);
				if (adresseInserted) {
					conn.commit();
					isInserted = true;
				} else {
					conn.rollback();
				}
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			logger.error("An error occurred while trying to insert " + client.getFirstname() + " " + client.getLastname() +
					" in the database.", e);
		}
		return isInserted;

	}

	private boolean addressStatementExecution(ClientAdr adr, Connection connection) throws SQLException {
		boolean isInserted = false;
		PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_ADRESSE);
		statement.setString(1, adr.getAddress());
		statement.setLong(2, adr.getCity().getIdCity());
		statement.setLong(3, adr.getIdClient());
		isInserted = statement.executeUpdate() != 0;
		return isInserted;
	}

	private long clientStatementExecution(ClientAdr adr, Client client, Connection conn) throws SQLException {
		long idClient = 0;

		PreparedStatement stmt = conn.prepareStatement(QUERY_INSERT_CLIENT, Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, client.getFirstname());
		stmt.setString(2, client.getLastname());
		stmt.setDate(3, Date.valueOf(client.getBirthdate()));  //YYYY-MM-DD
		stmt.setString(4, client.getEmail());

		String password = client.getPassword(); // this is the raw, unhashed password
		// we need to convert it into a hashed password
		try {
			String hashedPassword = HashCreator.generateStrongPasswordHash(password);
			stmt.setString(5, hashedPassword);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException e) {
			logger.error("An error occurred while trying to hash the password.", e);
		}

		stmt.setDate(6, Date.valueOf(LocalDate.now()));
		int affectedRows = stmt.executeUpdate();

		if (affectedRows > 0) {
			try (ResultSet resultSet = stmt.getGeneratedKeys()) {
				if (resultSet.next()) {

					idClient = resultSet.getLong(1);
					adr.setIdClient(idClient);
				}
			} catch (SQLException e) {
				conn.rollback();
				logger.error("An error occurred while trying to get the ID of the inserted client", e);
			}
		}

		return idClient;
	}

	/**
	 * Inserts a new <code>Employee</code> into the underlying database, based on the given arguments.
	 *
	 * @param employee the employee to insert
	 * @return <code>true</code> if the operation succeeded, <code>false</code> if it failed.
	 */
	@Override
	public boolean insertEmployee(Employee employee) {
		boolean isInserted = false;



		try(Connection conn = dataSource.getConnection()){
			conn.setAutoCommit(false);

			PreparedStatement stmt = conn.prepareStatement(QUERY_INSERT_EMPLOYEE);
			stmt.setString(1, employee.getFirstname());
			stmt.setString(2, employee.getLastname());
			stmt.setDate(3, Date.valueOf(employee.getBirthdate()));  //YYYY-MM-DD
			stmt.setString(4, employee.getEmail());

			String password = employee.getPassword(); // this is the raw, unhashed password
			// we need to convert it into a hashed password
			try {
				String hashedPassword = HashCreator.generateStrongPasswordHash(password);
				stmt.setString(5, hashedPassword);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException e) {
				logger.error("An error occurred while trying to hash the password.", e);
			}

			stmt.setDate(6, Date.valueOf(LocalDate.now()));
			stmt.setString(7, "E");
			int affectedRows = stmt.executeUpdate();
			if(affectedRows > 0){
				isInserted = true;
				conn.commit();
			}
			else{
				conn.rollback();
			}


		} catch (SQLException e) {
			logger.error("An error occurred while trying to insert " + employee.getFirstname() + " " + employee.getLastname() +
					" in the database.", e);

		}
		return isInserted;
	}

	/**
	 * Initiates a new <code>Client</code> session, by using the argument <code>login</code> and <code>password</code>.
	 *
	 * @param login the email address of the client that serves as its login.
	 * @param password the unhashed password of the client.
	 * @return <code>null</code> if authentication failed, the <code>Client</code> object that is associated with the argument
	 * <code>login</code> and <code>password</code> otherwise.
	 */
	@Override
	public Client loginClient(String login, String password) {
		Client client = null;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement statement = conn.prepareStatement(QUERY_LOGIN_CLIENT);
			statement.setString(1, login);
			statement.setString(2, HashCreator.generateStrongPasswordHash(password));
			ResultSet rs = statement.executeQuery();

			while (rs.next()){
				Long id = rs.getLong("id_client");
				client = new Client(
						id,
						rs.getString("lastname"),
						rs.getString("firstname"),
						rs.getDate("birthdate").toLocalDate(),
						rs.getString("mail"),
						rs.getString("hashed_password"),
						rs.getDate("signin").toLocalDate(),
						new ClientAdr(addressDao.getAddressInformations(id))
				);


				// If the Client already deactivated their account, login will fail
				rs.getDate("signout");
				boolean testnull = rs.wasNull();
				if (!testnull){
					client = null;
				}
			}



		}  catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException e) {
			logger.error("An error occurred whilst trying to authenticate", e);
		}

		return client;
	}

	/**
	 * Initiates a new <code>Employee</code> session, by using the argument <code>login</code> and <code>password</code>.
	 *
	 * @param login the email address of the employee that serves as its login.
	 * @param password the unhashed password of the employee.
	 * @return <code>null</code> if authentication failed, the <code>Employee</code> object that is associated with the argument
	 * <code>login</code> and <code>password</code> otherwise.
	 */
	@Override
	public Employee loginEmployee(String login, String password) {
		Employee employee = null;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement statement = conn.prepareStatement(QUERY_LOGIN_CLIENT);
			statement.setString(1, login);
			statement.setString(2, HashCreator.generateStrongPasswordHash(password));
			ResultSet rs = statement.executeQuery();

			while (rs.next()){
				Long id = rs.getLong("id_employee");
				employee = new Employee(
						id,
						rs.getString("lastname"),
						rs.getString("firstname"),
						rs.getDate("birthdate").toLocalDate(),
						rs.getString("mail"),
						rs.getString("hashed_password"),
						rs.getDate("signin").toLocalDate()

				);


				// If the Client already deactivated their account, login will fail
				rs.getDate("signout");
				boolean testnull = rs.wasNull();
				if (!testnull){
					employee = null;
				}
			}



		} catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException e) {
			logger.error("An error occurred whilst trying to authenticate", e);
		}

		return employee;
	}

	/**
	 * Closes the account represented by the currenly logged in <code>User</code>
	 *
	 * @param connectedClient the currently connected <code>User</code>
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 */
	@Override
	public boolean closeAccount(Client connectedClient) {
		boolean isDateInserted = false;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement statement = conn.prepareStatement(QUERY_INSERT_SIGNOUT_DATE_CLIENT);
			statement.setDate(1, Date.valueOf(LocalDate.now()));
			statement.setLong(2, connectedClient.getUserId());
			isDateInserted = statement.executeUpdate() > 0;

		} catch (SQLException e) {
			logger.error("An error occurred while trying to signout a client", e);
		}

		return isDateInserted;
	}

	/**
	 * Closes the account represented by the currenly logged in <code>User</code>
	 *
	 * @param connectedEmployee the currently connected <code>User</code>
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 */
	@Override
	public boolean closeAccount(Employee connectedEmployee) {
		boolean isDateInserted = false;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement statement = conn.prepareStatement(QUERY_INSERT_SIGNOUT_DATE_EMPLOYEE);
			statement.setDate(1, Date.valueOf(LocalDate.now()));
			statement.setLong(2, connectedEmployee.getUserId());
			isDateInserted = statement.executeUpdate() > 0;

		} catch (SQLException e) {
			logger.error("An error occurred while trying to signout a client", e);
		}

		return isDateInserted;
	}

	/**
	 * Updates the currently connected <code>Client</code> in the database with the informations
	 * stored in the argument <code>Client</code>.
	 *
	 * @param updatedClient the <code>Client</code> object to update the database with.
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 */
	@Override
	public boolean updateConnectedClient(Client updatedClient) {
		boolean isUpdated = false;

		try(Connection conn = dataSource.getConnection()) {
			PreparedStatement statement = conn.prepareStatement(QUERY_UPDATE_CLIENT);
			statement.setString(1, updatedClient.getLastname());
			statement.setString(2, updatedClient.getFirstname());
			statement.setString(3, updatedClient.getPassword());
			statement.setLong(4, updatedClient.getUserId());

			isUpdated = statement.executeUpdate() > 0;
		} catch (SQLException e) {
			logger.error("An error occurred while trying to update the database.", e);
		}

		return isUpdated;
	}

	/**
	 * Updates the currently connected <code>Employee</code> in the database with the informations
	 * stored in the argument <code>Employee</code>.
	 *
	 * @param updatedEmployee the <code>Employee</code> object to update the database with.
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 */
	@Override
	public boolean updateConnectedEmployee(Employee updatedEmployee) {
		boolean isUpdated = false;

		try(Connection conn = dataSource.getConnection()) {
			PreparedStatement statement = conn.prepareStatement(QUERY_UPDATE_EMPLOYEE);
			statement.setString(1, updatedEmployee.getLastname());
			statement.setString(2, updatedEmployee.getFirstname());
			statement.setString(3, updatedEmployee.getPassword());
			statement.setLong(4, updatedEmployee.getUserId());
			isUpdated = statement.executeUpdate() > 0;

		} catch (SQLException e) {
			logger.error("An error occurred while trying to update the database.", e);
		}

		return isUpdated;
	}
}
