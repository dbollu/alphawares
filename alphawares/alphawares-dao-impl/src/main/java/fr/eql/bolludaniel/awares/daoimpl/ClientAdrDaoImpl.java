package fr.eql.bolludaniel.awares.daoimpl;

import fr.eql.bolludaniel.awares.dao.ClientAdrDao;
import fr.eql.bolludaniel.awares.dao.UserDao;
import fr.eql.bolludaniel.awares.datasource.AlphaWaresDataSource;
import fr.eql.bolludaniel.awares.entity.City;
import fr.eql.bolludaniel.awares.entity.ClientAdr;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Remote(ClientAdrDao.class)
@Stateless
public class ClientAdrDaoImpl implements  ClientAdrDao{

	private final Logger logger = LogManager.getLogger();

	private final String REQ_RETRIEVE_ADDRESS =
			"SELECT a.id_address, a.address, c.id_city, c.pcode, c.city FROM address a, city c WHERE c.id_city = a.id_city AND a.id_client = ?";

	private final DataSource dataSource = new AlphaWaresDataSource();

	@Override
	public ClientAdr getAddressInformations(Long clientId) {

		ClientAdr address = null;

		try (Connection conn = dataSource.getConnection()) {

			PreparedStatement ps = conn.prepareStatement(REQ_RETRIEVE_ADDRESS);
			ps.setLong(1, clientId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				address = new ClientAdr(
						rs.getLong("id_address"),
						rs.getString("address"),
						new City(
							rs.getLong("id_city"),
							rs.getString("city"),
							rs.getString("pcode")
						),
						clientId
				);

			}

		} catch (SQLException e) {
			logger.error("An error occurred while trying to access the database.", e);
		}

		return address;
	}
}
