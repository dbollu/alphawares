package fr.eql.bolludaniel.awares.daoimpl;

import fr.eql.bolludaniel.awares.dao.ProductDao;
import fr.eql.bolludaniel.awares.datasource.AlphaWaresDataSource;
import fr.eql.bolludaniel.awares.entity.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Remote(ProductDao.class)
public class ProductDaoImpl implements ProductDao {

	private static final Logger logger = LogManager.getLogger();

	private static final String QUERY_RETRIEVE_ALL_PRODUCTS = "SELECT * FROM product";

	private static final String QUERY_INSERT_PRODUCT = "INSERT INTO product (name, available_qty, price) VALUES (?, ?, ?)";

	private static final String QUERY_UPDATE_PURCHASE = "UPDATE product SET available_qty = (available_qty - ?) WHERE name = ?";
	private static final String QUERY_UPDATE_RESTOCK = "UPDATE product SET available_qty = available_qty + ? WHERE name = ?";

	private final DataSource dataSource = new AlphaWaresDataSource();

	@Override
	public List<Product> retrieveAllProducts() {

		List<Product> products = new ArrayList<>();

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement sql = conn.prepareStatement(QUERY_RETRIEVE_ALL_PRODUCTS);
			ResultSet rs = sql.executeQuery();
			while (rs.next()){
				products.add(
						new Product(
								rs.getString("name"),
								rs.getFloat("price"),
								rs.getLong("product_id"),
								rs.getInt("available_qty")
						)
				);
			}

		} catch (SQLException e) {
			logger.error("An error occurred while attempting to retrive the list of products in the database", e);
		}


		return products;

	}

	@Override
	public boolean addProduct(String name, Float price, Integer qty) {
		boolean isInserted = false;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement sql = conn.prepareStatement(QUERY_INSERT_PRODUCT);
			sql.setString(1, name);
			sql.setFloat(2, price);
			sql.setInt(3, qty);
			isInserted = sql.executeUpdate() != 0; // true if a row is inserted, false otherwise
		} catch (SQLException e) {
			logger.error("An error occurred while attempting to insert a product in the database", e);
		}


		return isInserted;
	}

	@Override
	public boolean updateProductFollowingPurchase(Integer amount, String name) {
		boolean isUpdated = false;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement sql = conn.prepareStatement(QUERY_UPDATE_PURCHASE);
			sql.setInt(1, amount);
			sql.setString(2, name);
			isUpdated = sql.executeUpdate() > 0;
		} catch (SQLException e) {
			logger.error("An error occurred while attempting to update the database", e);
		}

		return isUpdated;
	}

	@Override
	public boolean updateProductFollowingRestock(Integer amount, String name) {
		boolean isUpdated = false;

		try(Connection conn = dataSource.getConnection()){
			PreparedStatement sql = conn.prepareStatement(QUERY_UPDATE_RESTOCK);
			sql.setInt(1, amount);
			sql.setString(2, name);
			isUpdated = sql.executeUpdate() > 0;
		} catch (SQLException e) {
			logger.error("An error occurred while attempting to update the database", e);
		}

		return isUpdated;
	}
}
