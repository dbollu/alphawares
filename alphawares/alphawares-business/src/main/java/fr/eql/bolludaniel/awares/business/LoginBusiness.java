package fr.eql.bolludaniel.awares.business;

import fr.eql.bolludaniel.awares.entity.Employee;

public interface LoginBusiness{

	Employee login(String login, String password);

}
