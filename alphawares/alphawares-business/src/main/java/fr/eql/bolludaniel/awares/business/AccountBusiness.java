package fr.eql.bolludaniel.awares.business;

import fr.eql.bolludaniel.awares.entity.Employee;

public interface AccountBusiness {

	boolean closeAccount(Employee employee);

	boolean updateConnectedEmployee(Employee employee);

}

