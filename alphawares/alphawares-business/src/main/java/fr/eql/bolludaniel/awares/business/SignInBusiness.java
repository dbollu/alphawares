package fr.eql.bolludaniel.awares.business;

import fr.eql.bolludaniel.awares.entity.Employee;

public interface SignInBusiness {

	boolean insertEmployee(Employee employee);

}
