package fr.eql.bolludaniel.awares.business;

public interface WaresBusiness{

	boolean restock(Integer qty, String name);

	boolean purchase(Integer qty, String name);

	List<Product> displayProducts();

}