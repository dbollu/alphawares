package fr.eql.bolludaniel.awares.dao;

import fr.eql.bolludaniel.awares.entity.Client;
import fr.eql.bolludaniel.awares.entity.ClientAdr;
import fr.eql.bolludaniel.awares.entity.Employee;


public interface UserDao {

	/**
	 * Sends a query to the underlying database to fetch the informations of a <code>Client</code>
	 * that is identified by the argument <code>firstname</code> and <code>lastname</code>.
	 *
	 * @return <code>null</code> if no match is found, the client's informations otherwise.
	 * */
	Client findClientInformations(String firstname, String lastname);

	/**
	 * Sends a query to the underlying database to fetch the informations of an <code>Employee</code>
	 * that is identified by the argument <code>firstname</code> and <code>lastname</code>.
	 *
	 * @return <code>null</code> if no match is found, the employee's informations otherwise.
	 * */
	Employee findEmployeeInformations(String firstname, String lastname);

	/**
	 * Inserts a new <code>Client</code> into the underlying database, based on the given arguments.
	 *
	 * @return <code>true</code> if the operation succeeded, <code>false</code> if it failed.
	 * */
	boolean insertClient(Client client, ClientAdr adr);

	/**
	 * Inserts a new <code>Employee</code> into the underlying database, based on the given arguments.
	 *
	 * @return <code>true</code> if the operation succeeded, <code>false</code> if it failed.
	 * */
	boolean insertEmployee(Employee employee);

	/**
	 * Initiates a new <code>Client</code> session, by using the argument <code>login</code> and <code>password</code>.
	 *
	 * @return <code>null</code> if authentication failed, the <code>Client</code> object that is associated with the argument
	 * <code>login</code> and <code>password</code> otherwise.
	 * */
	Client loginClient(String login, String password);

	/**
	 * Initiates a new <code>Employee</code> session, by using the argument <code>login</code> and <code>password</code>.
	 *
	 * @return <code>null</code> if authentication failed, the <code>Employee</code> object that is associated with the argument
	 * <code>login</code> and <code>password</code> otherwise.
	 * */
	Employee loginEmployee(String login, String password);

	/**
	 * Closes the account represented by the currenly logged in <code>Client</code>.
	 *
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 * */
	boolean closeAccount(Client connectedClient);

	/**
	 * Closes the account represented by the currenly logged in <code>Client</code>.
	 *
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 * */
	boolean closeAccount(Employee connectedEmployee);

	/**
	 * Updates the currently connected <code>Client</code> in the database with the informations
	 * stored in the argument <code>Client</code>.
	 *
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 * */
	boolean updateConnectedClient(Client updatedClient);

	/**
	 * Updates the currently connected <code>Employee</code> in the database with the informations
	 * stored in the argument <code>Employee</code>.
	 *
	 * @return <code>true</code> if the operation succeeds, <code>false</code> if it fails.
	 * */
	boolean updateConnectedEmployee(Employee updatedEmployee);

}
