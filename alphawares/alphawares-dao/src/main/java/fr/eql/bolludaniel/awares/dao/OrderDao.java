package fr.eql.bolludaniel.awares.dao;

import fr.eql.bolludaniel.awares.entity.Client;
import fr.eql.bolludaniel.awares.entity.Order;

import java.util.List;

public interface OrderDao {


		/**
		 * Retrieves all orders made by the argument <code>Client</code>. Archived orders are ignored.
		 * */
		List <Order> retrieveOrdersByClient(Client client);

		/**
		 * Retrieves all unarchived orders stored in the database.
		 * */
		List <Order> retrieveAllOrders();

		/**
		 * Adds a new <code>Order</code> object into the database.
		 * */
		boolean insertOrder(Order cart, Client client);

}
