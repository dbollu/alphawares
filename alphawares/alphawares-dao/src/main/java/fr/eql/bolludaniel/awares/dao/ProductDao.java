package fr.eql.bolludaniel.awares.dao;

import fr.eql.bolludaniel.awares.entity.Product;

import java.util.List;

public interface ProductDao {

	List<Product> retrieveAllProducts();

	boolean addProduct(String name, Float price, Integer qty);

	boolean updateProductFollowingPurchase(Integer amount, String name);

	boolean updateProductFollowingRestock(Integer amount, String name);


}
