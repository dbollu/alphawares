package fr.eql.bolludaniel.awares.dao;

import fr.eql.bolludaniel.awares.entity.ClientAdr;

public interface ClientAdrDao {

	ClientAdr getAddressInformations(Long clientId);

}
