package fr.eql.bolludaniel.awares.dao;

import fr.eql.bolludaniel.awares.entity.City;

public interface CityDao {

	City getCityInformations();

}
