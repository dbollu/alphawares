package fr.eql.bolludaniel.awares.businessimpl;

import fr.eql.bolludaniel.awares.business.AccountBusiness;
import fr.eql.bolludaniel.awares.dao.UserDao;
import fr.eql.bolludaniel.awares.entity.Employee;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(AccountBusiness.class)
@Stateless
public class AccountBusinessImpl implements AccountBusiness{

	@EJB
	UserDao userDao;

	@Override public boolean closeAccount(Employee employee){
		return userDao.closeAccount(employee);
	}

	@Override public boolean updateConnectedEmployee(Employee employee){
		return userDao.updateConnectedEmployee(employee);
	}

}