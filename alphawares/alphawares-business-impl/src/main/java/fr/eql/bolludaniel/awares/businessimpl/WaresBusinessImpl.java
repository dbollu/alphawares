package fr.eql.bolludaniel.awares.businessimpl;

import fr.eql.bolludaniel.awares.business.WaresBusiness;
import fr.eql.bolludaniel.awares.dao.ProductDao;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(WaresBusiness.class)
@Stateless
public class WaresBusinessImpl implements WaresBusiness{

	@EJB
	private ProductDao productDao;

	@Override public boolean restock(Integer qty, String name){
		return productDao.updateProductFollowingRestock(qty, name);
	}

	@Override public boolean purchase(Integer qty, String name){
		return productDao.updateProductFollowingPurchase(qty, name);
	}

	@Override public List<Product> displayProducts(){
		return productDao.retrieveAllProducts();
	}

}