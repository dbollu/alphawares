package fr.eql.bolludaniel.awares.businessimpl;

import fr.eql.bolludaniel.awares.business.SignInBusiness;
import fr.eql.bolludaniel.awares.dao.UserDao;
import fr.eql.bolludaniel.awares.entity.Employee;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(SignInBusiness.class)
@Stateless public class SignInBusinessImpl implements SignInBusiness{

	@EJB private UserDao userDao;

	@Override public boolean insertEmployee(Employee employee){
		return userDao.insertEmployee(employee);
	}

}
