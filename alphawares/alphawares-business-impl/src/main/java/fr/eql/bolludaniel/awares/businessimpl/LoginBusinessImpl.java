package fr.eql.bolludaniel.awares.businessimpl;

import fr.eql.bolludaniel.awares.business.LoginBusiness;
import fr.eql.bolludaniel.awares.dao.UserDao;
import fr.eql.bolludaniel.awares.entity.Employee;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(LoginBusiness.class)
@Stateless
public class LoginBusinessImpl implements LoginBusiness{

	@EJB
	private UserDao userDao;

	@Override public Employee login(String login, String password){
		return userDao.loginEmployee(login, password);
	}

}
